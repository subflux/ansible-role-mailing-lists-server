# Ansible role to install a complete Mailing-Lists server

## Introduction

This role installs and configure all needed softwares to get a complete
Mailing-Lists server with administrative webui and anti-spam.

The following softwares are used:

- Mailman 3
- Apache HTTPd
- Postfix
- Spamassassin
- Postgrey
- Memcached
- PostgreSQL

This role a simple TLS setup is `use_simple_tls` is set to True (see
parameter information below). If you need more customization, then
you can pass parameters to the underlying roles by passing them to
this one.

Configuration example:

    - hosts: lists.example.com
      roles:
      - role: mailing-lists-server
        display_name: "My Project List Archives"
        domain: example.com
        webui_vhost: lists.example.com
        admin_users:
          - jake
          - finn
        mail_aliases:
          # Person who should get root's mail
          root:
            - root
            - bubblegum@example.com
          listmaster: root
          princess: bubblegum@example.com

## Requirements

This role now depends on a version of the 'httpd' role implementing
split responsabilities, see:
  https://github.com/OSAS/ansible-role-httpd/pull/36

## Variables

- **display_name**: display name of this ML instance (in web UI)
- **domain**: mail domain
- **webui_vhost**: domain of the web vhost
                   defaults to the value of `domain` if not set
- **admin_users**: list of site administrators usernames
                   if defined, grant priviledges to these usernames and revoke to all others
                   if undefined, let the current priviledges untouched
- **mail_aliases**: mail aliases (like in /etc/aliases)
- **social_auth.{service_name}**: enable social authentication for systems requiring an API account
   each `service_name` (free name) has a `provider` (github, google…), a `display_name` as title in the web UI,
   as well as a `client_id` and `client_secret` to connect to the provider
- **use_simple_tls**: setup Let's Encrypt TLS certificate for the vhost.
                      also setup TLS for the MTA using the same certificate as the web vhost
                      (the MTA hostname will then be added in the certificate SAN)

